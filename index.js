const Discord = require("discord.js"); 
const yt = require('ytdl-core');
const randomcolor = require('randomcolor');
const Nitaka = new Discord.Client(); 


Nitaka.on('channelPinsUpdate', (channel, time) => {
	channel.guild.defaultChannel.sendMessage(`The pins for ${channel.name} Have been updated.`);
});



Nitaka.on("guildMemberAdd", (member) => {
    console.log(`Welcome ${member.user.username} to ${member.guild.name}` );
    member.guild.defaultChannel.sendMessage(`Welcome ${member.user.username} to ${member.guild.name}`);
});



Nitaka.on("guildMemberRemove", (member) => {
   console.log(`${member.user.username} left ${member.guild.name}` );
   member.guild.defaultChannel.sendMessage(`${member.user.username} has left ${member.guild.name}.`);
   
   });

  
  Nitaka.on("ready", () => {
  console.log("Nitaka is up ^-^");
  Nitaka.user.setStatus("online"); 
  Nitaka.user.setGame("Sprinkling cakes"); 
});


Nitaka.on("message", (message) => {


  var SubAPI = message.content.toUpperCase(); 
   
  let command = message.content.toUpperCase().split(" ")[0];
 
  let args = message.content.split(" ").slice(1);
  
  let Broad = message.content.split(" ").slice(0);
  
  
  
   if (SubAPI == "+UPTIME") {
    var date = new Date(Nitaka.uptime);
    var strDate = '**';
    strDate += 'Uptime\n';
    strDate += date.getUTCDate() - 1 + ' days, ';
    strDate += date.getUTCHours() + ' hours, ';
    strDate += date.getUTCMinutes() + ' minutes, ';
    strDate += date.getUTCSeconds() + ' seconds**';
    message.channel.sendMessage(strDate)
  }
  
  
  
   if (SubAPI === "+BROADCAST") {
    let broadcastmessage = broad;
 Nitaka.guilds.forEach(x => x.defaultChannel.sendMessage(broad))
  } //This doesn't work 
  
  
  
    if (SubAPI == "+USERINFO") {
      var user = message.mentions.users.first();
      var embed = new Discord.RichEmbed();
      if (!user) {
        embed.addField("Username", `${message.author.username}#${message.author.discriminator}`, true)
            .addField("ID", `${message.author.id}`, true)
            .setColor(randomcolor())
            .setFooter(' ', ' ')
            .setThumbnail(`${message.author.avatarURL}`)
            .setTimestamp()
            .setURL(`${message.author.avatarURL}`)
            .addField('Currently', `${message.author.presence.status}`, true)
            .addField('Game', `${message.author.presence.game === null ? "No Game" : message.author.presence.game.name}`, true)
            .addField('Joined Discord', `${moment(message.author.createdAt).format('MM.DD.YY')}`, true)
            .addField('Joined Server', `${moment(message.member.joinedAt).format('MM.DD.YY')}`, true)
            .addField('Roles', `\`${message.member.roles.filter(r => r.name).size}\``, true)
            .addField('Is Bot', `${message.author.bot}`, true)
            message.channel.sendEmbed(
                embed, {
                    disableEveryone: true
                }
            );
        } else {
        embed.addField("Username", `${user.username}#${user.discriminator}`, true)
            .addField("ID", `${user.id}`, true)
            .setColor(randomcolor())
            .setFooter(' ', ' ')
            .setThumbnail(`${user.avatarURL}`)
            .setTimestamp()
            .setURL(`${user.avatarURL}`)
            .addField('Currently', `${user.presence.status}`, true)
            .addField('Game', `${user.presence.game === null ? "No Game" : user.presence.game.name}`, true)
            .addField('Joined Discord', `${moment(user.createdAt).format('MM.DD.YY')}`, true)
            .addField('Is Bot', `${user.bot}`, true)
      message.channel.sendEmbed(
          embed, {
              disableEveryone: true
          }
      );
    }
  } else

    if (SubAPI == "+SERVERINFO ") {
      var embed = new Discord.RichEmbed();
      embed.addField("Server Name", `${message.guild.name}`, true)
          .addField("Server ID", `${message.guild.id}`, true)
          .setColor(randomcolor())
          .setFooter(' ', ' ')
          if (message.guild.iconURL) embed.setThumbnail(`${message.guild.iconURL}`);
          if (message.author.avatarURL) embed.setURL(`${message.author.avatarURL}`);
      embed.setTimestamp()
          .addField('Guild Owner', `${message.guild.owner.user.username}`, true)
          .addField('Owner ID', `${message.guild.owner.id}`, true)
          .addField('Guild Created', `${moment(message.guild.createdAt).format('MM.DD.YY')}`, true)
          .addField('Roles', `${message.guild.roles.filter(r => r.name).size}`, true)
          .addField('Verification Level', `${message.guild.verificationLevel}`, true)
          .addField('Region', `${message.guild.region}`, true)
      message.channel.sendEmbed(
          embed, {
              disableEveryone: true
          }
      );
  } 
  
  
     if (SubAPI == "+STATS") {
      var embed = new Discord.RichEmbed();
      embed.setColor(randomcolor())
          .setFooter(' ', ' ')
          if (Nitaka.user.avatarURL) embed.setThumbnail(`${Nitaka.user.avatarURL}`);
      embed.setTimestamp()
          .addField('Servers', `${Nitaka.guilds.size}`, true)
          .addField('Users', `${Nitaka.users.size}`, false)
          .addField('Discord Version', `${Discord.version}`, false)
          .addField('Uptime', (Math.round(Nitaka.uptime / (1000 * 60 * 60))) + " hours, " + (Math.round(Nitaka.uptime / (1000 * 60)) % 60) + " minutes, and " + (Math.round(Nitaka.uptime / 1000) % 60) + " seconds")
      message.channel.sendEmbed(
          embed, {
              disableEveryone: true
          }
      );
  }
  
  
  
  
  
  
  
  
    if (SubAPI == "+PING") {
    startTime = Date.now();
    message.channel.sendMessage("Pinging...").then((message) => {
      endTime = Date.now();
      message.edit(Math.round(endTime - startTime) + " -ms");
      });
  }
  
  
  
  if (command === "+SAY") {
    message.channel.sendMessage(args.join(" ")).catch(console.error);
  }
  
  

  
  
    if (command === "+SKIN") {
    message.channel.sendFile('https://visage.surgeplay.com/skin/'+args.join(" "),'SKINFILEOK.png')};
	
	
	  
    if (command === "+3DSKIN") {
    message.channel.sendFile('https://visage.surgeplay.com/full/512/'+args.join(" "),'SKINFILEOK.png')};
   
    if (command === "+NAMEMC") {
    message.channel.sendMessage('https://namemc.com/s/'+args.join(" "))};
   
   
   if (SubAPI ===  "NITAKA, REMOTE CONTROL"){
   Nitaka.user.setAvatar("./pics/PlusAndMinus.jpg")
   message.channel.sendEmbed({ color: 3447003,
  description: "L R L R STOP & DASH & UP & TALK" }) 
    };

    if (SubAPI === "NITAKA, MAIN"){
   Nitaka.user.setAvatar("https://s-media-cache-ak0.pinimg.com/originals/c2/32/92/c23292d9bea79c3c751ee722f929b73e.jpg")
 message.channel.sendEmbed({ color: 3447003,
  description: ":)" }); 
     };

     if (SubAPI === "NITAKA, NORMALITY"){
   Nitaka.user.setAvatar("./pics/normal.jpg")
   message.channel.sendEmbed({ color: 3447003,
  description: "Changed to my main profile picture :heart:" });
     };


 if (SubAPI === "CLOD")
{
if (message.author.id == "150655187491618816")
{
    message.channel.sendFile('https://cdn.discordapp.com/attachments/221225278465114112/278657501535141888/Clods.gif','OwO.gif')
}}


      if (SubAPI === "NITAKA, OLD IDENTITY"){
   Nitaka.user.setAvatar("./pics/Old_icon.jpg")
   message.channel.sendEmbed({ color: 3447003,
  description: "Went back to my old profile picture, I look cute here don't I?"}); 
     };

   if (SubAPI === "NITAKA, REFRESH"){
   Nitaka.user.setAvatar("http://68.media.tumblr.com/6994841d552ad24434d964291441ad2c/tumblr_inline_n98e2zpR8x1ss34qm.gif")
   message.channel.sendEmbed({ color: 3447003,
  description: "Refreshed."}); 
     };


 if (SubAPI === "HAIL!"){
message.channel.sendFile('http://stream1.gifsoup.com/view4/1978196/heil-hitler-o.gif','OwO.gif')
}


if (message.content.toUpperCase().startsWith("+KICK ")) {
    let userToKick = message.mentions.users.first();
    message.guild.member(userToKick).kick();
message.reply("I haz removed ${member.user.username}");
}

if (message.content.toUpperCase().startsWith("+BAN ")) {
    let userToKick = message.mentions.users.first();
    message.guild.member(userToKick).ban();
message.reply("Banned.");
}


if (message.content.toUpperCase().includes("CARLY RAE JEPSEN")) {
  message.reply("Carly is the best musician out there!");
}


if (SubAPI === "WHO IS NITAKA?") {
message.channel.sendMessage("", {embed: {
  color: 3447003,
  author: {
    name: Nitaka.user.username,
    icon_url: Nitaka.user.avatarURL
  },
  title: 'Nitaka bio',
  url: 'http://twitter.com/imsubdivide',
  description: 'EMBED EMBED EMBEDS!',
  fields: [
    {
      name: 'Who Made Nitaka?',
      value: 'I was coded by <@178562619886665728> ^-^'
    },
    {
      name: 'Where can I contact them?',
      value: 'Type +contactinfo for more information andd links (Not yet implemented)'
    },
    {
      name: 'What version are you?',
      value: 'I am the node.js version. A Java version using the JDA API is coming soon and will probably be more powerful.'
    },
    {
      name: 'What can you do??',
      value: 'Type +help for more information on that :)'
    }
  ],
  timestamp: new Date(),
  footer: {
    icon_url: Nitaka.user.avatarURL,
    text: 'SubAPI'
  }
}}); 
     }
	 
	 
	 
	 
	 
	 
if(SubAPI === "NITAKA, LET ME INVITE YOU!") //I keep the invite seperate from the Array of replise for Conveinience 
   message.reply("https://discordapp.com/oauth2/authorize?client_id=274590190729822208&scope=bot&permissions=221772920");

    if (SubAPI ===  "NITAKA, MIND SENDING ME MY AVATAR?") {
   message.channel.sendFile(message.author.avatarURL ,'OwO.png')}
   
 if (message.content.toUpperCase().startsWith(Nitaka.user+" HUG ME!")) {
message.reply('Okii! **HUGS**')
message.channel.sendFile('http://cdn.smosh.com/sites/default/files/ftpuploads/bloguploads/0413/epic-hugs-toy-story.gif','OwO.gif')}


 if (SubAPI ===  "+AVATAR") {
message.channel.sendFile(message.author.avatarURL ,'OwO.png')}

if (SubAPI ===  "WHO IS MUMBLES?") {
message.channel.sendFile('https://cdn.discordapp.com/attachments/209620687818588161/279613536378945537/MUMBLES.png','MUMBLESNOTHATSTHEFUCKINGCARPETNO.png')}

  
 if(SubAPI === "+INVITE") {
 message.reply("https://discordapp.com/oauth2/authorize?//client_id=274590190729822208&scope=bot&permissions=221772920");
}

message.author.sendMessage("", {embed: {
  color: 3447003,
  author: {
    name: Nitaka.user.username,
    icon_url: Nitaka.user.avatarURL
  },
  title: 'Nitaka Help',
  url: 'http://twitter.com/imsubdivide',
  description: 'Below you will see my various commands! :D (Case sensitivity no longer matters, type them in broken caps all you like)',
  fields: [
    {
      name: '+avatar',
      value: 'This will send you a image of your avatar to the chat!'
    },
	    {
      name: '+ping',
      value: 'Get Ping.'
    },
	    {
      name: '+stats',
      value: 'Get information about Nitaka'
    },
	    {
      name: '+uptime',
      value: 'Get how long Nitaka has been awake without a restart'
    },
 {
      name: 'Can I ask questions?',
      value: 'Yeah! Just: @Nitaka tell me + Your question (Without the +) (A +ask function will later be added for mobile users...)'
    },
    {
      name: '+Kick <username>',
      value: 'This will remove people from the chat, Currently requires you to have a certain permission (being worked on)'
    },
    {
      name: '+skin MCUsername',
      value: 'This will send a players Minecraft skin file to the chat.'
    },
    {
      name: '+3Dskin MCUsername',
      value: 'This will send a 3D Image of a Minecraft Player to the chat.'
    },
    {
      name: '+Namemc MCUsername',
      value: 'Allows you to quickly search names on https://namemc.com; Just saves time really.'
    } 
  ],
  timestamp: new Date(),
  footer: {
    icon_url: Nitaka.user.avatarURL,
    text: 'SubAPI'
  }
}});    
message.channel.sendMessage("I Sent ya a dm!");


}

     if (SubAPI === "WAR_MAJOR") {
     responses = ["WHERE EVER YOU SEE HIM, GAZER AIN'T FAR BEHIND","HE HAS A SIMPSONS OBSESSION", "ONE OF THE NICEST STAFF MEMBERS", "###_#####",
    "Oh god...irish jokes inbound....", "#IRISH","That sexy Irish guy?"]
    message.reply(responses[Math.floor(Math.random() * 6 - 1)]);
  }

  
 if (SubAPI === "+DELETE")
{
if (message.author.id == "178562619886665728")
{
    message.channel.bulkDelete(100); //This is copy pasted twice because you can"t go over 100 for some reason...
    message.channel.sendMessage("100 messages were Deleted :heart:");
   } 
else if (message.author.id !== "178562619886665728")
{
  message.channel.sendMessage("You're not allowed to do this. Sorry.");

  }
 }


  if (message.content.startsWith('+musictest')) {
    const voiceChannel = message.member.voiceChannel;
    if (!voiceChannel) {
      return message.reply(`Please be in a voice channel first!`);
    }
    voiceChannel.join()
      .then(connnection => {
        let stream = yt("https://www.youtube.com/watch?v=JTcrJZ6rsm8", {audioonly: true});
        const dispatcher = connnection.playStream(stream);
        dispatcher.on('end', () => {
          voiceChannel.leave();
        });
      });
  }


   
  if (message.content.toUpperCase().startsWith(Nitaka.user) {
    responses = ["It is certain", "It is decidedly so", "Without a doubt",
    "Yes, definitely", "You may rely on it", "As I see it, yes", "Most likely",
    "Outlook good", "Yes", "Signs point to yes", "Reply hazy try again",
    "Ask again later", "Better not tell you now", "Cannot predict now",
    "Concentrate and ask again", "Don't count on it", "My reply is no",
    "My sources say no", "Outlook not so good", "Very doubtful"]
    message.reply(responses[Math.floor(Math.random() * 20 - 1)]);
  }
    
       if (SubAPI ===  "NOOT NOOT!") {
    responses = ["https://giphy.com/gifs/pingu-noot-gif-gfJtgKZrBfXP2 ", "https://giphy.com/gifs/noot-x0AW5CkJ2Akuc ", "https://giphy.com/gifs/pingu-noot-gif-pKZvvnJoFUh8Y ",
    "https://giphy.com/gifs/clapping-clap-pingu-QBC5foQmcOkdq ", "I'm nooter-ed out", "https://giphy.com/gifs/pingu-noot-gif-sy23M3VeBGkvK ", "https://giphy.com/gifs/KJOfv5OePYoBq "]
    message.reply(responses[Math.floor(Math.random() * 20 - 1)]);
  }

  });
  function NitakaRepliesChannel(message) {
  if (typeof message !== "string") return undefined;
  return NitakaRepliesChannel[message.content.toUpperCase()];
}

Nitaka.on("message", (message) => {
  var response = NitakaRepliesChannel(message);
  if (response) {
    Nitaka.channel.sendMessage(message, response);
  }
});
  
  
  
  
  
  
  
  

  var NitakaRespond = {
  "AYY": "Ayy, lmao!",
  "OI NITAKA WYD":"AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA",
  "KYS":"Would you like the bleach to go or are you drinking here?",
  "THANKS NITAKA!":":)",
  "NIGHT NITAKA": "Goodnight All!",
  "WHO IS BELLATORHD?":":bell: :door: :regional_indicator_h: :regional_indicator_d:",  
  "WHO MADE THE DALEK MOD?":"The big nosed <@186179645379444736> did :)",
  "WAT": "Say what?",
  "12 Edustries": "https://i.gyazo.com/2b68798b778498e080f64f941f7216ec.gif",
  "SUPPORT!": "I SUPPORT THIS!",
  "WHO IS FORD?": "Ford is <@205481812485472258>, he makes models for the Dalek Mod.",
  "LOL_": "What's so funny?",
  "NITAKA, SAY SORRY": "Sorry! >->",
  "I'M SAD": ":frowning:",
  "HEY NITAKA, WHO DOES SUB LOVE THE MOST?": "Dunno >->",
  "NITAKA, WANT TO HUG?": "**EYES LIGHT UP** YES I LOVE CUDDLES! **CUDDLES YA**",
  "HEY HANDLES, CAN YOU REMIND ME TO REROUTE THE PHONE TO THE MAIN CONSOLE": "Handles is too dumb to answer that >->",
  "NOT LISTENING.": "https://s-media-cache-ak0.pinimg.com/564x/72/5a/67/725a67173b2d459ca69df323c4062afe.jpg",
  "GOOD BOT! HAVE A COOKIE!": "YAY",
  "NITAKA-CHAAAAN": "**EYES LIGHT UP** YES SENPAI?!",
  "MOO!":"QUICK HIDE ALL THE COWS, MATT MIGHT SEE EM",
  "NITAKA, SHOW ME YOUR NOOTS": "http://i.imgur.com/Uy0P8Nt.gif",
  "WHO IS LORD?": "Malum is Lord of Darkness!...Hes terrified of light....",
  "WHAT IS BEST DRINK?": "Tea of course!",
  "WHO IS WAR?": "A Silly Irish Pleb!",
  "HI!": "Heya!",
  "GRRRRR": "BARK BARK!",
 "NITAKA, WHO IS THE BEST SISTER?": "That Gei girl called Kim! (<@241559521770733568>) I wonder if she learned how to exit multiplayer yet.....",
 "NITAKA, WHERE ARE YOU?": "I'm trapped in a command prompt on a server in the US..Please send help...Handles is giving me looks...",
 "THIS WILL END IN TEARS...":"I'll go get the tissues then...",
 "NITAKA, YOU GOT THE DRUGS??!!?": "YES BUT SHHH, THE COPPERS WILL GET US!",
 "LEWD!": "https://media.tenor.co/images/afcdbdd2cdbcaef5184a59d9c1025a0a/raw",
 "NO FUCKS GIVEN": "https://media.giphy.com/media/3o85xGocUH8RYoDKKs/giphy.gif",
 "88 MILES PER HOUR!": "http://i842.photobucket.com/albums/zz346/savagehenry71/Movies/2dl53et.gif",
 "WHO IS SPACE?": "How am I suppoed to know?",
 "HOLY SHIT!":"https://media.giphy.com/media/KayVJ5lkB84rm/giphy.gif",
 "I THINK JOHN'S ILL OR KIDNAPPED":"yeah...I think so too..."
};


function getResponse(message) {
  return NitakaRespond[message.content.toUpperCase()];
}

Nitaka.on('message', (message) => {
  var response = getResponse(message);
  if (response) {
    message.channel.sendMessage(response);
	console.log('Someone got a reply from the array!');
  }
});




var request = require('request');
var mcCommand = 'DMU Up?'; // Command for triggering
var mcIP = 'dmu.swdteam.co.uk'; // Your MC server IP
var mcPort = 25564; // Your MC server port

Nitaka.on('message', message => {
    if (message.content === mcCommand) {
        var url = 'http://mcapi.us/server/status?ip=' + mcIP + '&port=' + mcPort;
        request(url, function(err, response, body) {
            if(err) {
                console.log(err);
                return message.reply('Error getting Minecraft server status...');
            }
            body = JSON.parse(body);
            var status = '*offline*';
            if(body.online) {
                status = '**DMU** is **online**  -  ';
                if(body.players.now) {
                    status += '**' + body.players.now + '** people are playing!';
                } else {
                    status += '*Nobody is playing!*';
                }
            }
            message.reply(status);
        });
    }
});



Nitaka.login('Mjc0NTkwMTkwNzI5ODIyMjA4.C20Tjg.cpY9gdUqN1qSSkyBQRqLJ7Bb9cA');
